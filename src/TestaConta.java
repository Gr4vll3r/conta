public class TestaConta {
    public static void main(String [] args) {
        ContaCorrente cc1 = new ContaCorrente(22,1,"Banco AA",100.00,550.00);
        cc1.saca(700.00);
        System.out.println("--------");
        System.out.println(cc1);
        System.out.println("--------");
        System.out.println("O saldo da conta corrente é R$"+cc1.getSaldo());
        System.out.println("--------");
        ContaPoupanca cp2 = new ContaPoupanca(33,3,"Banco CCC",265.00,17,0.05);
        cp2.deposita(100.00);
        System.out.println("O saldo da conta Poupança é R$"+cp2.getSaldo());
        System.out.println("--------");
        ContaSalario cs3 = new ContaSalario(45,5,"Banco SSS",855.55);
        cs3.saca(50.00);
        System.out.println("O Saldo da conta salário é R$"+cs3.getSaldo());
        System.out.println("--------");
        cs3.saca(50.00);
    }
}

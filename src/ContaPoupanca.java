public class ContaPoupanca extends Conta{
    private  int diaAniversario;
    private double taxaDeJuros;

    public ContaPoupanca(int numero, int agencia, String banco, double saldo, int diaAniversario, double taxaDeJuros) {
        super(numero, agencia, banco, saldo);
        this.diaAniversario = diaAniversario;
        this.taxaDeJuros = taxaDeJuros;
    }
    public double getSaldo(){
        if(this.diaAniversario == 17 )
            return this.saldo*this.taxaDeJuros;
        return this.saldo;
    }
    @Override
    void deposita(double quantidade) {
        super.deposita(quantidade);
    }
    @Override
    void saca(double quantidade) {
        super.saca(quantidade);
    }

}

public class ContaSalario extends Conta {
    private int limite = 0;
    public ContaSalario(int numero, int agencia, String banco, double saldo) {
        super(numero, agencia, banco, saldo);
    }
    @Override
    public double getSaldo() {
        return this.saldo;
    }
    @Override
    public String toString() {
        return "ContaSalario{" +
                "saldo=" + saldo +
                '}';
    }
    @Override
    void deposita(double quantidade) {
        super.deposita(quantidade);
    }
    @Override
    void saca(double quantidade) {
        this.limite++;
        if (this.limite >1) {
            System.out.println("Voçê excedeu o limite de saque!");
            return;
        }
        super.saca(quantidade);
    }
}

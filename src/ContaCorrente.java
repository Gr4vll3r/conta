public class ContaCorrente extends Conta{
    private double chequeEspecial;
    public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
        super(numero, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;
    }
    @Override
    public String toString() {
        return "ContaCorrente{" +
                "chequeEspecial=" + chequeEspecial +
                '}';
    }
    public double getSaldo(){
        return this.chequeEspecial + this.saldo;
    }
    @Override
    void deposita(double quantidade) {
        super.deposita(quantidade);
    }
    @Override
    void saca(double quantidade) {
        if (quantidade >this.getSaldo()) {
            System.out.println("Voçê não pode efetuar saques maior que o valor permitido!");
            return;
        }
        super.saca(quantidade);
    }
}
